package ru.tfs.jmiddle.shop;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.math.BigDecimal;
import java.util.UUID;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.tfs.jmiddle.shop.config.ExceptionsControllerAdvice;
import ru.tfs.jmiddle.shop.config.ObjectMapperConfiguration;
import ru.tfs.jmiddle.shop.controller.ItemsController;
import ru.tfs.jmiddle.shop.domain.Item;
import ru.tfs.jmiddle.shop.repository.ItemsRepository;
import ru.tfs.jmiddle.shop.repository.SimpleItemsRepository;
import ru.tfs.jmiddle.shop.service.ItemsMapperImpl;
import ru.tfs.jmiddle.shop.service.ItemsServiceImpl;

@SpringBootTest(classes = {
    ItemsController.class,
    ItemsServiceImpl.class,
    SimpleItemsRepository.class,
    ItemsMapperImpl.class,
    ExceptionsControllerAdvice.class,
    ObjectMapperConfiguration.class
})
public class ItemsControllerTest {

    private static final String URL = "/api/v1/items";

    @Autowired
    private ItemsRepository repository;
    @Autowired
    private ItemsController controller;

    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        repository.deleteAll();;
        mockMvc = MockMvcBuilders.standaloneSetup(controller).alwaysDo(MockMvcResultHandlers.print()).build();
    }

    @Test
    public void findAllTest() throws Exception {
        Item item = new Item();
        item.setId(UUID.randomUUID());
        item.setPrice(BigDecimal.TEN);
        item.setName("milk");
        repository.save(item);

        mockMvc.perform(MockMvcRequestBuilders.get(URL))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", Matchers.hasSize(1)))
            .andExpect(jsonPath("$.[0].id").value(item.getId().toString()))
            .andExpect(jsonPath("$.[0].price").value(item.getPrice()))
            .andExpect(jsonPath("$.[0].name").value(item.getName()));
    }

    @Test
    public void getByIdTest() throws Exception {
        Item item = new Item();
        item.setId(UUID.randomUUID());
        item.setPrice(BigDecimal.TEN);
        item.setName("milk");
        repository.save(item);

        mockMvc.perform(MockMvcRequestBuilders.get(URL + "/{id}", item.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(item.getId().toString()))
            .andExpect(jsonPath("$.price").value(item.getPrice()))
            .andExpect(jsonPath("$.name").value(item.getName()));
    }

    @Test
    public void getByIdTest_negative() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get(URL + "/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }
}
