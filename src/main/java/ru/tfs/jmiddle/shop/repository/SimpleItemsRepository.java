package ru.tfs.jmiddle.shop.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Repository;
import ru.tfs.jmiddle.shop.domain.Item;

@Repository
public class SimpleItemsRepository implements ItemsRepository {

    private final Map<UUID, Item> items = new ConcurrentHashMap<>();

    @Override
    public Item save(Item item) {
        items.put(item.getId(), item);
        return item;
    }

    @Override
    public List<Item> findAll() {
        return items.values().stream().toList();
    }

    @Override
    public boolean existByName(String name) {
        return items.values()
            .stream()
            .map(Item::getName)
            .anyMatch(n -> n.equals(name));
    }

    @Override
    public Optional<Item> findById(UUID id) {
        return Optional.ofNullable(items.get(id));
    }

    @Override
    public void delete(Item item) {
        items.remove(item.getId());
    }

    @Override
    public void deleteAll() {
        items.clear();
    }
}
