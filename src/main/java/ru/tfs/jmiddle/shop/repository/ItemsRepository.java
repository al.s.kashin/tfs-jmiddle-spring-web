package ru.tfs.jmiddle.shop.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import ru.tfs.jmiddle.shop.domain.Item;

public interface ItemsRepository {

    Item save(Item item);
    List<Item> findAll();
    boolean existByName(String name);
    Optional<Item> findById(UUID id);
    void delete(Item item);
    void deleteAll();
}
