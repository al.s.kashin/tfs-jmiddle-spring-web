package ru.tfs.jmiddle.shop.controller;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.tfs.jmiddle.shop.dto.ItemCreateRequest;
import ru.tfs.jmiddle.shop.dto.ItemDto;
import ru.tfs.jmiddle.shop.dto.ItemUpdateRequest;
import ru.tfs.jmiddle.shop.service.ItemsService;

@RestController
@RequestMapping("api/v1/items")
@RequiredArgsConstructor
public class ItemsController {

    private final ItemsService itemsService;

    @GetMapping
    public List<ItemDto> findAll() {
        return itemsService.findAll();
    }

    @GetMapping("/{id}")
    public ItemDto getById(@PathVariable UUID id) {
        return itemsService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ItemDto create(@Validated @RequestBody ItemCreateRequest request) {
        return itemsService.create(request);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ItemDto update(@PathVariable UUID id, @Validated @RequestBody ItemUpdateRequest request) {
        return itemsService.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        itemsService.delete(id);
    }
}
