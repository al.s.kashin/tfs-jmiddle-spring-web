package ru.tfs.jmiddle.shop.service;

import java.util.List;
import java.util.UUID;
import ru.tfs.jmiddle.shop.dto.ItemCreateRequest;
import ru.tfs.jmiddle.shop.dto.ItemDto;
import ru.tfs.jmiddle.shop.dto.ItemUpdateRequest;

public interface ItemsService {

    List<ItemDto> findAll();
    ItemDto getById(UUID id);
    ItemDto create(ItemCreateRequest request);
    ItemDto update(UUID id, ItemUpdateRequest request);
    void delete(UUID id);
}
