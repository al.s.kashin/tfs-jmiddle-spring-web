package ru.tfs.jmiddle.shop.service;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import ru.tfs.jmiddle.shop.domain.Item;
import ru.tfs.jmiddle.shop.dto.ItemCreateRequest;
import ru.tfs.jmiddle.shop.dto.ItemDto;
import ru.tfs.jmiddle.shop.dto.ItemUpdateRequest;

@Mapper(componentModel = "spring")
public interface ItemsMapper {

    ItemDto toItemResponse(Item item);

    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID())")
    Item toItem(ItemCreateRequest request);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "name", ignore = true)
    Item updateItem(@MappingTarget Item item, ItemUpdateRequest request);
}
