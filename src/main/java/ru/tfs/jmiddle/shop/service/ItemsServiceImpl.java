package ru.tfs.jmiddle.shop.service;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tfs.jmiddle.shop.domain.Item;
import ru.tfs.jmiddle.shop.dto.ItemCreateRequest;
import ru.tfs.jmiddle.shop.dto.ItemDto;
import ru.tfs.jmiddle.shop.dto.ItemUpdateRequest;
import ru.tfs.jmiddle.shop.exceptions.BadRequestException;
import ru.tfs.jmiddle.shop.exceptions.EntityNotFoundException;
import ru.tfs.jmiddle.shop.repository.ItemsRepository;

@Service
@RequiredArgsConstructor
public class ItemsServiceImpl implements ItemsService {

    private final ItemsRepository repository;
    private final ItemsMapper mapper;

    @Override
    public List<ItemDto> findAll() {
        return repository.findAll()
            .stream()
            .map(mapper::toItemResponse)
            .toList();
    }

    @Override
    public ItemDto getById(UUID id) {
        Item item = getItem(id);
        return mapper.toItemResponse(item);
    }

    @Override
    public ItemDto create(ItemCreateRequest request) {
        if (repository.existByName(request.getName())) {
            throw new BadRequestException("Item with name [" + request.getName() + "] already exist");
        }

        Item item = mapper.toItem(request);
        item = repository.save(item);
        return mapper.toItemResponse(item);
    }

    @Override
    public ItemDto update(UUID id, ItemUpdateRequest request) {
        Item item = getItem(id);
        item = mapper.updateItem(item, request);
        item = repository.save(item);
        return mapper.toItemResponse(item);
    }

    @Override
    public void delete(UUID id) {
        Item item = getItem(id);
        repository.delete(item);
    }

    private Item getItem(UUID id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Item", id));
    }

}
