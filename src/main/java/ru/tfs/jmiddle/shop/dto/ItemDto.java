package ru.tfs.jmiddle.shop.dto;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemDto {
    private UUID id;
    private String name;
    private BigDecimal price;
}
