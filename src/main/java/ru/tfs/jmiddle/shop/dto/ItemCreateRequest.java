package ru.tfs.jmiddle.shop.dto;

import java.math.BigDecimal;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemCreateRequest {
    @NotBlank
    private String name;
    @NotNull
    @Min(value = 0)
    private BigDecimal price;
}
