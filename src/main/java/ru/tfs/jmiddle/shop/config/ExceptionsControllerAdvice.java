package ru.tfs.jmiddle.shop.config;

import java.time.LocalDateTime;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.tfs.jmiddle.shop.exceptions.BadRequestException;

@ControllerAdvice
public class ExceptionsControllerAdvice {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> handleException(BadRequestException exception) {
        String message = String.format("%s: %s", LocalDateTime.now(), exception.getMessage());
        return ResponseEntity.badRequest().body(Map.of("error", message));
    }
}
