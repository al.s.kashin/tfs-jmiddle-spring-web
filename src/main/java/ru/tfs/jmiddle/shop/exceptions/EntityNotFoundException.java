package ru.tfs.jmiddle.shop.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EntityNotFoundException extends ResponseStatusException {

    private static final HttpStatus STATUS = HttpStatus.NOT_FOUND;

    public EntityNotFoundException(String entityCode, Object id) {
        super(STATUS, String.format("Entity %s with id=%s not found", entityCode, id.toString()));
    }
}
